//musical notes for sounds
#define C6 523
#define D6 587
#define E6 659
#define F6 698

int sounds[4] = { C6, D6, E6, F6 }; //sounds array

int audio_pin = 12; //pin for buzzer
int leds_pins[4] = { 6, 8, 9, 10 }; //pins for leds
int buttons_pins[4] = { 2, 3, 4, 5 }; //pins for buttons

int sequence[100] = {}; //sequence to be repeated
int current_step_sequence = 0; //it is used to count the steps in the sequence array
int current_round = 0; // it indicates the current round in the game
int pressed_button = 0;
int game_over = false;


void setup() {

  for (int i = 0; i <= 3; i++) { //setting the LED pins as output
    pinMode(leds_pins[i], OUTPUT);
  }

  for (int i = 0; i <= 3; i++) { //setting the Buttons pins as input
    pinMode(buttons_pins[i], INPUT);
  }

  pinMode(audio_pin, OUTPUT); //setting buzzer pin as output

  randomSeed(analogRead(0)); //starting random sequence
}


void loop() {

  if (game_over) {

    sequence[100] = {};
    current_step_sequence = 0;
    current_round = 0;
    game_over = false;
  }


  if (current_round == 0) { //it plays a sound to indicate game started

    initialSound();
    delay(500);
  }

  nextRound();
  playSequence(); //play sequence to be repeated
  waitingPlayer(); //waiting for the player's answer
  delay(1000);
}


void initialSound() {

  tone(audio_pin, sounds[0]);

  digitalWrite(leds_pins[0], HIGH);
  digitalWrite(leds_pins[1], HIGH);
  digitalWrite(leds_pins[2], HIGH);
  digitalWrite(leds_pins[3], HIGH);

  delay(500);

  digitalWrite(leds_pins[0], LOW);
  digitalWrite(leds_pins[1], LOW);
  digitalWrite(leds_pins[2], LOW);
  digitalWrite(leds_pins[3], LOW);

  delay(500);
  noTone(audio_pin);
}


void nextRound() { //draw a new item and add it in the sequence

  int number_drawn = random(0, 4);
  sequence[current_round++] = number_drawn;
}


void playSequence() { //sound sequences to be repeated by the player

  for (int i = 0; i < current_round; i++) {

    tone(audio_pin, sounds[sequence[i]]);
    digitalWrite(leds_pins[sequence[i]], HIGH);
    delay(500);

    noTone(audio_pin);
    digitalWrite(leds_pins[sequence[i]], LOW);
    delay(100);
  }

  noTone(audio_pin);
}


void waitingPlayer() { //waiting for player to start the turn

  for (int i = 0; i < current_round; i++) {

    gameStatus();
    checkingPlay();

    if (game_over) {
      break;
    }

    current_step_sequence++;

  }

  current_step_sequence = 0;
}


void gameStatus() {

  boolean round_done = false;

  while (!round_done) {

    for (int i = 0; i <= 3; i++) {

      if (digitalRead(buttons_pins[i]) == HIGH) {
        pressed_button = i;

        tone(audio_pin, sounds[i]);
        digitalWrite(leds_pins[i], HIGH);
        delay(300);

        digitalWrite(leds_pins[i], LOW);
        noTone(audio_pin);

        round_done = true;
      }
    }

    delay(10);
  }
}


void checkingPlay() {

  if (sequence[current_step_sequence] != pressed_button) {

    for (int i = 0; i <= 3; i++) {
      tone(audio_pin, sounds[i]);
      digitalWrite(leds_pins[i], HIGH);
      delay(200);

      digitalWrite(leds_pins[i], LOW);
      noTone(audio_pin);
    }

    noTone(audio_pin);
    game_over = true;
  }
}
